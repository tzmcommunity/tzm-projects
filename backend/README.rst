==============
How to set up:
==============

After cloning, env is installed from the Python 3 standard library. It is presumed that Python 3 is bound to python command. If it is not, you can use pyenv for that, but future docker support will come as well.

.. code-block:: console

    python -m venv env

The env activates with sourcing the profile file that you can also inspect:

.. code-block:: console

    source profile

FYI, it deactivates with the command:

.. code-block:: console

    deactivate

To install the dependencies (the env must be active for the rest of the steps):

.. code-block:: console

    pip install -r requirements.txt

When developing, if a new package needs to be installed:

.. code-block:: console

    pip install somepackage

And freeze the installed dependencies to a requirements.txt that already exists:

.. code-block:: console

    pip freeze > requirements.txt

The file above will get split in the future for development, production and testing.

To start the django project after installing the dependencies, go into the backend folder (where the manage.py is located) and run:

.. code-block:: console

    python manage.py runserver

The project ca then be accessed in a browser on http://localhost:8000/
The admin url exists in development and a new user can be created through the shell:

.. code-block:: console

    python manage.py createsuperuser

This will change for the production with the usage of the proper RDBMS and static and media serving, but for now it's meant to be used locally only.

Let's get this off the ground!
