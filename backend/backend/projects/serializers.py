from rest_framework import serializers

from .models import Activity, Project


class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = ("id", "timestamp", "description", "project_id")


class ProjectSerializer(serializers.ModelSerializer):
    activity_set = ActivitySerializer(read_only=True, many=True)

    def to_representation(self, instance):
        response = super(ProjectSerializer, self).to_representation(instance)
        if instance.image:
            response['image'] = instance.image.url
        return response

    class Meta:
        model = Project
        fields = (
            "id",
            "title",
            "image",
            "description",
            "hidden",
            "activity_set",
            "members",
            "contributions",
            "location"
        )
