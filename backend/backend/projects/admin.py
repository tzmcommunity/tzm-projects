from django.contrib import admin

from .models import Activity, Project


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    pass


@admin.register(Activity)
class ActivityAdmin(admin.ModelAdmin):
    pass
