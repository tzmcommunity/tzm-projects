from django.contrib.auth import get_user_model
from django.db import models


class Project(models.Model):
    title = models.CharField(max_length=255)
    image = models.ImageField(blank=True, null=True, upload_to="images/")
    description = models.TextField()
    hidden = models.BooleanField(default=True)
    owner = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, null=True)
    contributions = models.BooleanField(default=True)
    location = models.CharField(max_length=255)

    class Meta:
        verbose_name = "Project"
        verbose_name_plural = "Projects"

    def __str__(self):
        return self.title


class Activity(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    description = models.TextField()

    class Meta:
        verbose_name = "Activity"
        verbose_name_plural = "Activities"

    def __str__(self):
        return " - ".join([str(self.project), str(self.timestamp)])
